<?php

use App\MasterGatewayOperations;

use Illuminate\Database\Seeder;

class MasterGatewayOperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterGatewayOperations::truncate();
        $operations = [
        		['operation' => 'Send Message'],
        	];
        foreach ($operations as $key => $operation) {
        	MasterGatewayOperations::create($operation);
        }
    }
}
