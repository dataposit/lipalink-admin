<?php

use App\User;
use App\UserType;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add the user types into the system
         */
        UserType::truncate();
        $usertypes = [
                ['name' => 'System Administrator', 'level' => 1],// Accessible over the administrator portal
                ['name' => 'Company Administrator', 'level' => 2],// Accessible over the customer portal
                ['name' => 'Paybill User', 'level' => 2],// Accessible over the customer portal
                ['name' => 'API User', 'level' => 3],// Accessible over the API
            ];
        foreach ($usertypes as $key => $usertype) {
            UserType::create($usertype);
        }


        /*
         * Truncate the users if only the initial system users are the only existing users in the system
         */
    	if (User::whereNotNull('organization_id')->count() == 0)
    	   User::truncate();


        /*
         * Create the default system admin user and the API User
         */
        // System admin insert
        if (User::where('email', '=', 'admin@gmail.com')->get()->isEmpty())
            $fac = factory(User::class, 1)->create([
                        'name' => 'Admin User',
                        'email' => 'admin@gmail.com',
                        'user_type_id' => 1
                    ]);

        // API User insert
        if (User::where('email', '=', 'apiuser@gmail.com')->get()->isEmpty())
            $fac = factory(App\User::class, 1)->create([
                    'name' => 'API User',
                    'email' => 'apiuser@gmail.com',
                    'user_type_id' => 4
                ]);

    }
}
