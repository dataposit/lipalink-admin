<?php

use Illuminate\Database\Seeder;
use App\ShortcodeType;

class ShortcodeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShortcodeType::truncate();
        $types = [
        	['type' => 'Buy Goods'],
        	['type' => 'Paybill'],
        	['type' => 'Lipa Mpesa Online'],
        ];
        ShortcodeType::insert($types);
    }
}
