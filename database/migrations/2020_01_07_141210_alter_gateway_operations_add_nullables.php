<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGatewayOperationsAddNullables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gateway_operations', function(Blueprint $table){
            $table->string('action')->nullable()->after('master_gateway_operation_id');
            $table->tinyInteger('requires_auth')->nullable()->after('action');
            $table->string('route')->nullable()->after('requires_auth');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
