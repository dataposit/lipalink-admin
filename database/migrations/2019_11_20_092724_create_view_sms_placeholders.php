<?php

use Illuminate\Database\Migrations\Migration;

class CreateViewSmsPlaceholders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( 'CREATE OR REPLACE VIEW `view_sms_placeholders` AS SELECT payments.id, mobile AS `customer_number`, NAME AS `customer_name`, payment_code AS `mpesa_code`, bill_reference AS `account`, payment_time, payment_amount AS `amount`, shortcode, short_name AS `mpesa_organization_name`, organization_name FROM payments JOIN shortcodes ON shortcodes.id = payments.shortcode_id JOIN organizations ON organizations.id = shortcodes.organization_id' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS view_sms_placeholders');
    }
}
