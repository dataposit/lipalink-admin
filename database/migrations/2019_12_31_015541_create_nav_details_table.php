<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nav_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('organization_id');
            $table->string('NTLM_URI')->nullable();
            $table->string('NTLM_PORT')->nullable();
            $table->string('NTLM_SERVICE')->nullable();
            $table->tinyInteger('NTLM_SOAP')->default(0);
            $table->tinyInteger('NTLM_ODATA')->default(0);
            $table->string('NTLM_ODATA_VERSION')->nullable();
            $table->string('NTLM_COMPANY')->nullable();
            $table->string('NTLM_USERNAME')->nullable();
            $table->string('NTLM_PASSWORD')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nav_details');
    }
}
