<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShortcodesAddPasskey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shortcodes', function(Blueprint $table){
            $table->dropColumn(['type', 'shortcode_type_id']);
            $table->string('passkey')->nullable();
            // $table->tinyInteger('type')->nullable()->after();
            // $table->tinyInteger('shortcode_type_id')->nullable()->after();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
