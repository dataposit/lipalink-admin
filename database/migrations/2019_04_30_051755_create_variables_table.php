<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->float('working_account', 9, 2)->nullable();
            $table->float('float_account', 9, 2)->nullable();
            $table->float('utility_account', 9, 2)->nullable();
            $table->float('charges_paid', 9, 2)->nullable();
            $table->float('org_settlement_account', 9, 2)->nullable();
            $table->timestamp('last_refreshed')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variables');
    }
}
