<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewayOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gateway_operations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('gateway_id');
            $table->integer('master_gateway_operation_id');
            $table->string('action');
            $table->tinyInteger('requires_auth');
            $table->string('route');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gateway_operations');
    }
}
