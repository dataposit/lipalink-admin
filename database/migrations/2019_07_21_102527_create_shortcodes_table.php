<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShortcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shortcodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('shortcode')->unique();
            $table->tinyInteger('type');
            $table->string('short_name');
            $table->bigInteger('shortcode_type_id');
            $table->bigInteger('organization_id');
            $table->text('confirm_callback')->nullable();
            $table->text('validate_callback')->nullable();
            $table->text('forward_call')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shortcodes');
    }
}
