<?php

namespace App;

use App\BaseModel;
use Carbon\Carbon;

class Payment extends BaseModel
{

	public function getPaymentDateAttribute()
	{
		return Carbon::create($this->payment_time)->format('Y-m-d');
	}

    public function getTransactions() {
    	if (auth()->user()->user_type_id == 1){
    		return $this->all();
    	} else {
    		$transactions = auth()->user()->organization->paybills;
    		return $transactions;
    	}
    }

    public function shortcode()
    {
    	return $this->belongsTo('App\Shortcode');
    }
}
