<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

use \App\Payment;

class Mpesa
{
	public static $url = 'https://api.safaricom.co.ke';
	// public static $self_url = 'http://197.248.9.51:8080' . '/api/callbacks/';
	public static $self_url = '';
	public static $test_number = '254708374149';
	public static $apiTokenPrefix = 'api_token_';

	function __construct()
	{
		self::$self_url = env('NGROK_URL') . '/api/callbacks/';
	}

	public static function get_token($online=0, $shortcode=null)
	{
		if(!Cache::store('file')->has(self::$apiTokenPrefix.$shortcode->shortcode)) {
			self::authenticate($online, $shortcode);
		}
		return Cache::store('file')->get(self::$apiTokenPrefix.$shortcode->shortcode);
	}

	public static function get_security_credential($initiator=null)
	{
		// return env('SECURITY_CREDENTIAL');
		// if(!$initiator) $initiator = env('MPESA_INITIATOR');
		if(!$initiator) $initiator = env('MPESA_SEC_CRED');
		$public_key = file_get_contents(public_path('mpesa_key.txt'));
		openssl_public_encrypt($initiator, $encrypted, $public_key, OPENSSL_PKCS1_PADDING);
		return base64_encode($encrypted);
	}

    public static function authenticate($online=0, $shortcode=null)
    {
        $client = new Client(['base_uri' => self::$url]);
    	$consumer_key = $shortcode->consumer_key ?? env('CONSUMER_KEY');
    	$consumer_secret = $shortcode->consumer_secret ?? env('CONSUMER_SECRET');
    	$encoded = base64_encode($consumer_key . ':' . $consumer_secret);

		$response = $client->request('get', '/oauth/v1/generate?grant_type=client_credentials', [
			'headers' => [				
				'Accept' => 'application/json',
				'Authorization' => 'Basic ' . $encoded,
			],
			'http_errors' => false,
		]);
		
		if($response->getStatusCode() > 399) die();

		$body = json_decode($response->getBody());
		
		$expires_in = $body->expires_in;
		// Cache::store('file')->put('api_token', $body->access_token, $body->expires_in);
		// $api_token_name = 'api_token';
		return Cache::store('file')->put('api_token_'.$shortcode->shortcode, $body->access_token, (60*60));
    }


    public static function check_balance()
    {
        $client = new Client(['base_uri' => self::$url]);

        $data = [
				'Initiator' => env('MPESA_INITIATOR'),
				'SecurityCredential' => self::get_security_credential(),
				'CommandID' => 'AccountBalance',
				'PartyA' => env('MPESA_SHORTCODE_TWO'),
				'IdentifierType' => '4',
				'Remarks' => 'Checking for balance',
				'QueueTimeOutURL' => self::$self_url . 'queue_timeout',
				'ResultURL' => self::$self_url . 'balance',
			];

		// dd($data);

		$response = $client->request('post', 'mpesa/accountbalance/v1/query', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(),
				'Accept' => 'application/json',
			],
			'http_errors' => false,
			'debug' => true,
			'json' => $data,
		]);

		$body = json_decode($response->getBody());

		if($response->getStatusCode() == 200){
			session(['toast_message' => 'The request for the Mpesa balance was successful.']);
		}
		else{
			session(['toast_error' => 1, 'toast_message' => 'The request for the Mpesa balance was not successful. Status code ' . $response->getStatusCode()]);			
		}
		print_r($body);
		echo  'Status code is ' . $response->getStatusCode();
		return;
    }

    public static function sim_c2b($shortcode = null, $mobile = null, $amount = null)
    {
        $client = new Client(['base_uri' => self::$url]);

		$response = $client->request('post', 'mpesa/c2b/v1/simulate', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(0,$shortcode),
			],
			'http_errors' => false,
			'json' => [
				'ShortCode' => $shortcode->shortcode ?? env('MPESA_SHORTCODE_TWO'),
				'CommandID' => 'CustomerPayBillOnline',
				'Amount' => $amount ?? rand(500, 20000),
				'Msisdn' => $mobile ?? self::$test_number,
				'BillRefNumber' => '3',
			],
		]);

		$body = json_decode($response->getBody());
		// if ($response->getStatusCode() == 200)
		// 	return true;
		// else
		// 	return false;
		echo 'Status code is ' . $response->getStatusCode();
		print_r($body);
    }

    public static function register_tinker($shortcode)
    {
    	$shortcode = Shortcode::where('shortcode', '=', $shortcode)->first();
    	dd(self::register_callback($shortcode));
    }

    public static function register_callback($data=null)
    {
    	$client = new Client(['base_uri' => self::$url]);
		$response = $client->request('post', 'mpesa/c2b/v1/registerurl', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(0,$data),
				'Accept' => 'application/json',
			],
			'http_errors' => false,
			'debug' => false,
			'json' => [
				'ShortCode' => $data->shortcode ?? env('MPESA_SHORTCODE_TWO'),
				'ResponseType' => 'Completed',
				// 'CommandID' => 'RegisterURL',
				'ConfirmationURL' => $data->confirm_callback ?? self::$self_url . 'confirm',
				'ValidationURL' => $data->validate_callback ?? self::$self_url . 'validate_transaction',
			],
		]);

		$body = json_decode($response->getBody());
		echo 'Status code is ' . $response->getStatusCode();
		print_r($body);
		return $response->getStatusCode();
    }

	// http://197.248.9.51:8080/api/callbacks/validate_transaction
    public static function sim($mobile=null, $amount=null, $shortcode=null)
    {
    	$shortcode = Shortcode::where('shortcode', $shortcode)->first();
    	// dd(env('NGROK_URL') . 'callback');
    	$payment = new Payment;
    	$payment->payment_amount = $amount ?? 10;
    	$payment->mobile = $mobile;
    	$payment->user_id = 1;
    	$payment->shortcode_id = $shortcode->shortcode;
    	$payment->save();

    	return self::lipa_na_mpesa($payment, $shortcode);
    }

    public static function lipa_na_mpesa($payment, $shortcode=null)
    {
        $client = new Client(['base_uri' => self::$url]);
        
        $timestamp = date('YmdHis', strtotime($payment->created_at));
        $mpesashortcode = $shortcode->shortcode ?? env('MPESA_SHORTCODE');
        $passkey = $shortcode->passkey ?? env('MPESA_PASSKEY');
        $base_encode = base64_encode($mpesashortcode . $passkey . $timestamp);
        $response = $client->request('post', 'mpesa/stkpush/v1/processrequest', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(null, $shortcode),
			],
			'http_errors' => false,
			'json' => [
				'BusinessShortCode' => $mpesashortcode,
				'Password' => $base_encode,
				'Timestamp' => $timestamp,
				'TransactionType' => 'CustomerPayBillOnline',
				'Amount' => $payment->payment_amount,
				'PartyA' => $payment->mobile,
				'PartyB' => $mpesashortcode,
				'PhoneNumber' => $payment->mobile,
				'CallBackURL'=> env('NGROK_URL') . 'callback',
				'AccountReference' => 'Account',
				'TransactionDesc' => 'Payment to Org',
			],
		]);
		// print_r($response);
		$body = json_decode($response->getBody());
		echo 'Status code is ' . $response->getStatusCode();
		// print_r($body);die();
		if($response->getStatusCode() == 200){
			$payment->merchant_request_id = $body->MerchantRequestID;
			$payment->checkout_request_id = $body->CheckoutRequestID;
			$payment->save();
			$return = true;
		}
		else{
			$return = false;
		}
		print_r($body);
		return $return;
    }

    // Lipa na mpesa query
    public static function transaction_status($payment)
    {
    	if(is_int($payment)) $payment = \App\Payment::findOrFail($payment);
        $client = new Client(['base_uri' => self::$url]);

        $timestamp = date('YmdHis', strtotime($payment->created_at));

		$response = $client->request('post', 'mpesa/stkpushquery/v1/query', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(),
			],
			'http_errors' => false,
			'json' => [
				'BusinessShortCode' => env('MPESA_SHORTCODE'),
				'Password' => base64_encode(env('MPESA_SHORTCODE') . env('MPESA_PASSKEY') . $timestamp),
				'Timestamp' => $timestamp,
				'CheckoutRequestID' => $payment->checkout_request_id,
			],
		]);

		$body = json_decode($response->getBody());
		echo 'Status code is ' . $response->getStatusCode();

		print_r($body);    	
    }



    public static function other_transaction_status($payment)
    {
    	if(is_int($payment)) $payment = \App\Payment::findOrFail($payment);
        $client = new Client(['base_uri' => self::$url]);

		$response = $client->request('post', 'mpesa/transactionstatus/v1/query', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(),
			],
			'http_errors' => false,
			'json' => [
				'Initiator' => env('MPESA_INITIATOR'),
				'SecurityCredential' => env('SECURITY_CREDENTIAL'),
				'CommandID' => 'TransactionStatusQuery',
				'TransactionID' => $payment->payment_code,
				'PartyA' => env('MPESA_SHORTCODE_TWO'),
				'IdentifierType' => '1',
				'Remarks' => 'Checking for transaction status',
				'QueueTimeOutURL' => self::$self_url . 'queue_timeout',
				'ResultURL' => self::$self_url . 'result',
			],
		]);

		$body = json_decode($response->getBody());
		echo 'Status code is ' . $response->getStatusCode();
		print_r($body);
    }



    public static function reverse_transaction($payment)
    {
    	if(is_int($payment)) $payment = \App\Payment::findOrFail($payment);
        $client = new Client(['base_uri' => self::$url]);

        $timestamp = date('YmdHis', strtotime($payment->created_at));

		$response = $client->request('post', 'mpesa/reversal/v1/request', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(),
			],
			'http_errors' => false,
			'json' => [
				'CommandID' => 'TransactionReversal',
				'Initiator' =>  env('MPESA_INITIATOR'),
				'SecurityCredential' => self::get_security_credential(),
				'TransactionID' => $payment->payment_code,
				'Amount' => $payment->payment_amount,
				'ReceiverParty' => $payment->mobile,
				'RecieverIdentifierType' => '1',
				'ResultURL' => self::$self_url . 'result',
				'QueueTimeOutURL' => self::$self_url . 'queue_timeout',
				'Remarks' => 'Please',
				'Occasion' => 'Work',
			],
		]);

		$body = json_decode($response->getBody());
		echo 'Status code is ' . $response->getStatusCode();

		dd($body);

		if($response->getStatusCode() == 200){
			$payment->merchant_request_id = $body->MerchantRequestID;
			$payment->checkout_request_id = $body->CheckoutRequestID;
			$payment->save();
		}
		print_r($body);
    }


    public static function sim_b2c()
    {
    	$payment = new Payment;
    	$payment->payment_amount = rand(100, 20000);
    	$payment->mobile = $mobile ?? self::$test_number;
    	$payment->user_id = 1;
    	$payment->save();

    	self::b2c($payment);    	
    }

    public static function sim_b2b()
    {
    	$payment = new Payment;
    	$payment->payment_amount = rand(100, 20000);
    	// $payment->mobile = $mobile ?? self::$test_number;
    	$payment->user_id = 1;
    	$payment->save();

    	echo self::b2b($payment);    	
    }

    public static function b2c($payment)
    {
        $client = new Client(['base_uri' => self::$url]);

        $timestamp = date('YmdHis', strtotime($payment->created_at));

		$response = $client->request('post', 'mpesa/b2c/v1/paymentrequest', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(),
			],
			'http_errors' => false,
			'json' => [
				// Command ID options
				// SalaryPayment
				// BusinessPayment
				// PromotionPayment
				'CommandID' => 'SalaryPayment',
				'InitiatorName' =>  env('MPESA_INITIATOR'),
				'SecurityCredential' => self::get_security_credential(),
				'Amount' => $payment->payment_amount,
				'PartyA' => env('MPESA_SHORTCODE_TWO'),
				'PartyB' => $payment->mobile,
				'Remarks' => 'Please',
				'Occasion' => 'Work',
				'ResultURL' => self::$self_url . 'result',
				'QueueTimeOutURL' => self::$self_url . 'queue_timeout',
			],
		]);

		$body = json_decode($response->getBody());
		echo 'Status code is ' . $response->getStatusCode();

		// dd($body);
		print_r($body);
		return;

		if($response->getStatusCode() == 200){
			$payment->merchant_request_id = $body->MerchantRequestID;
			$payment->checkout_request_id = $body->CheckoutRequestID;
			$payment->save();
		}
    }



    public static function b2b($payment)
    {
        $client = new Client(['base_uri' => self::$url]);

        $timestamp = date('YmdHis', strtotime($payment->created_at));

		$response = $client->request('post', 'mpesa/b2b/v1/paymentrequest', [
			'headers' => [
				'Authorization' => 'Bearer ' . self::get_token(),
			],
			'http_errors' => false,
			// 'debug' => true,
			'json' => [
				// Command ID options
				// BusinessPayBill
				// BusinessBuyGoods
				// DisburseFundsToBusiness
				// BusinessToBusinessTransfer
				// BusinessTransferFromMMFToUtility
				// MerchantToMerchantTransfer
				// MerchantTransferFromMerchantToWorking
				// MerchantServicesMMFAccountTransfer
				// AgencyFloatAdvance
				'CommandID' => 'BusinessToBusinessTransfer',
				'Initiator' =>  env('MPESA_INITIATOR'),
				'SecurityCredential' => self::get_security_credential(),
				'Amount' => $payment->payment_amount,
				'PartyA' => env('MPESA_SHORTCODE_TWO'),
				'PartyB' => env('MPESA_SHORTCODE_THREE'),
				'SenderIdentifier' => '4',
				'SenderIdentifierType' => '4',
				'RecieverIdentifierType' => '4',
				'Remarks' => 'Please',
				'Occasion' => 'Work',
				'ResultURL' => self::$self_url . 'result',
				'QueueTimeOutURL' => self::$self_url . 'queue_timeout',
			],
		]);

		$body = json_decode($response->getBody());
		echo 'Status code is ' . $response->getStatusCode();

		print_r($body);
		return;

		if($response->getStatusCode() == 200){
			$payment->merchant_request_id = $body->MerchantRequestID;
			$payment->checkout_request_id = $body->CheckoutRequestID;
			$payment->save();
		}
    }

    public static function checkIdentity()
    {

    }


}
