<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

use App\Mpesa;
use Exception;

class BaseController extends Controller
{
    use Helpers;

    public function dump_log($name)
    {
    	if(!is_dir(storage_path('app/logs/'))) mkdir(storage_path('app/logs/'), 0777);

		$postData = file_get_contents('php://input');
		
		$file = fopen(storage_path('app/logs/' . $name .'.txt'), "a");
		if(fwrite($file, $postData) === FALSE) fwrite("Error: no data written");
		fwrite($file, "\r\n");
		fclose($file);


		try {
			$postData = json_decode($postData);
			return $postData;
		} catch (Exception $e) {
			print_r($e);
		}
		return $postData;
    }

    public function convert_time($param, $format='Y-m-d H:i:s')
    {	
    	if (!isset($param))
    		return NULL;
    	$d = \DateTime::createFromFormat('YmdHis', $param);
    	return $d->format($format);
    }

}