<?php

namespace App\Api\V1\Controllers;

// use BaseController;
use App\Api\V1\Controllers\BaseController;
use App\Api\V1\Requests\MpesaRequest;
use Exception;
use App\Lookup;
use App\Payment;
use App\User;
use App\SMS;
use App\Shortcode;

class MpesaController extends BaseController
{

	public function queue_timeout(MpesaRequest $request)
	{
		$this->dump_log('mpesa_queue_timeout');
	}

	// Mpesa balance response
	public function balance(MpesaRequest $request)
	{
		$r = $this->dump_log('mpesa_balance');
		$result = $r->Result;

		if($result->ResultType == 0 && $result->ResultCode == 0){
			$bal = $result->ResultParameters->ResultParameter[0]->Value;

			$values = explode('|', $bal);

			$v = \App\Variable::first();
			if(!$v) $v = new Variable;

			$data = [
				'last_refreshed' => $this->convert_time($result->ResultParameters->ResultParameter[1]->Value),
				'working_account' => $values[2],
				'float_account' => $values[7],
				'utility_account' => $values[12],
				'charges_paid' => $values[17],
				'org_settlement_account' => $values[22],
			];	

			$v->fill($data);
			$v->save();

			die();
		}
	}

	// Mpesa
	public function result(MpesaRequest $request)
	{
		$r = $this->dump_log('mpesa_result_two');
		die();
		$result = $r->Result;

		if($result->ResultType == 0 && $result->ResultCode == 0){
			$bal = $result->ResultParameters->ResultParameter[0]->Value;
			// $d = $result->ResultParameters->ResultParameter[1]->Value;
			// $d = \DateTime::createFromFormat('YmdHis', $d);

			$values = explode('|', $bal);

			$v = \App\Variable::first();

			$data = [
				// 'last_refreshed' => $d->format('Y-m-d H:i:s'),
				'last_refreshed' => $this->convert_time($result->ResultParameters->ResultParameter[1]->Value),
				'working_account' => $values[2],
				'float_account' => $values[7],
				'utility_account' => $values[12],
				'charges_paid' => $values[17],
				'org_settlement_account' => $values[22],
			];	

			$v->fill($data);
			$v->save();

			die();
		}
	}

	public function confirm(MpesaRequest $request, $shortcode = null)
	{
		$body = $this->dump_log('mpesa_confirm');
		$shortcode = Shortcode::where('shortcode', $shortcode)->first();

		$p = Payment::create([
			'payment_code' => $request->input('TransID'),
			'shortcode_id' => $shortcode->id ?? null,
			'mobile' => $request->input('MSISDN'),
			'bill_reference' => $request->input('BillRefNumber'),
			'payment_amount' => $request->input('TransAmount'),
			'name' => $request->input('FirstName') . ' ' . $request->input('MiddleName') .  ' ' . $request->input('LastName'),
			'payment_time' => $this->convert_time($request->input('TransTime')),
			'payment_status_id' => 2,
		]);		
	}


	// Incoming mpesa transactions
	public function validate_transaction(MpesaRequest $request, $shortcode = null)
	{
		$this->dump_log('mpesa_validate');
		$shortcode = Shortcode::where('shortcode', $shortcode)->first();
		// print_r($shortcode);die();
		$p = new Payment;
		$p->fill([
			'payment_code' => $request->input('TransID'),
			'shortcode_id' => $shortcode->id ?? null,
			'mobile' => $request->input('MSISDN'),
			'bill_reference' => $request->input('BillRefNumber'),
			'payment_amount' => $request->input('TransAmount'),
			'name' => $request->input('FirstName') . ' ' . $request->input('MiddleName') .  ' ' . $request->input('LastName'),
			'payment_time' => $this->convert_time($request->input('TransTime')),
			'payment_status_id' => 2,
		]);

		if(starts_with($p->bill_reference, 'acc-')){
			$u = User::find(str_after($p->bill_reference, 'acc-'));
			if($u) $p->user_id = $u->id;
		}

		$p->save();
		// print_r($p);die();

		$phone = $request->input('MSISDN') ?? null;
		$message = "Your payment of Kes." . $p->payment_amount . " has been received. Thank you for shopping with us";
		if ($phone == '254708374149' || $phone == null)
			$phone = '254725455925';
		SMS::testsms($phone, $message);
		return response()->json(['status' => 'ok', 'code' => 200]);
	}

	// Being the callback after a lipa na mpesa query
	public function callback(MpesaRequest $request)
	{
		$body = $this->dump_log('mpesa_callback');

		$body = $body->Body->stkCallback;
		$p = Payment::where(['checkout_request_id' => $body->CheckoutRequestID, 'merchant_request_id' => $body->MerchantRequestID])->first();

		if($body->ResultCode != 0){
			$p->payment_status_id = 3;
			$p->save();
		}
		else{
			$values = $body->CallbackMetadata->Item;
			$p->fill([
				'payment_time' => $values[3]->Value,
				'payment_code' => $values[1]->Value,
				'payment_status_id' => 2,
			]);
			$p->save();
			$message = "Your payment of Kes." . $p->payment_amount . " has been received. Thank you for shopping with us";
			$phone = $values[4]->Value;
			if ($phone == '254708374149')
				$phone = '254725455925';
			SMS::testsms($values[4]->Value, $message);
		}
	}

}