<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

use \App\Payment;

class Eazzy
{
	// public static $url = 'https://api.equitybankgroup.com/identity-sandbox/v1';
	public static $url = 'https://sandbox.jengahq.io';
	// public static $url = '';
	public static $self_url = 'https://36865706.eu.ngrok.io' . '/api/callbacks/';

	public static function get_token()
	{
		if(Cache::store('file')->has('api_token')){}
		else{
			self::authenticate();
		}
		return Cache::store('file')->get('api_token');
	}

	public static function create_keys()
	{
		exec("openssl genrsa -out " . public_path('privkey.pem') . " 2048 -nodes");
		exec("openssl rsa -in " . public_path('privkey.pem') . " -outform PEM -pubout -out " . public_path('pubkey.pem'));
	}

    public static function authenticate()
    {
        $client = new Client(['base_uri' => self::$url]);

        $data = [
        	'username' => env('EQUITY_USERNAME'),
        	'password' => env('EQUITY_PASSWORD'),
        ];

		$response = $client->request('post', '/identity-test/v2/token', [
			'headers' => [				
				'Accept' => 'application/json',
				'Authorization' => 'Basic ' . env('EQUITY_APIKEY'),
			],
			// 'debug' => true,
			'http_errors' => false,
			'form_params' => $data,
		]);

		// if($response->getStatusCode() > 399) die();

		$body = json_decode($response->getBody());
		// echo 'Response code is ' . $response->getStatusCode() . "\n";
		dd($body);
		$token_type = $body->token_type;
		$issued_at = $body->issued_at;
		$token = $body->access_token;
		$expires_in = ((int) $body->expires_in) - 10;
		Cache::store('file')->put('api_token', $body->access_token, $expires_in);
    }


}
