<?php

namespace App;

class NavDetails extends BaseModel
{
    public function organization()
    {
    	return $this->belongsTo('App\Organization', 'organization_id');
    }
}
