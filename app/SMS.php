<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class SMS
{
	private static $url = "https://n4qvy.api.infobip.com";
	private static $test_phone = "254725455925";

    public static function testsms($phone_number=NULL, $message=NULL) {
    	$client = new Client(['base_uri' => self::$url]);
        $response = $client->request('post', '/sms/1/text/single', [
			// 'debug' => true,
			'headers' => [				
				'Accept' => 'application/json',
				'Authorization' => 'Basic ' . base64_encode(env('INFOBIP_USERNAME') . ':' . env('INFOBIP_PASSWORD')),
			],
			'http_errors' => false,
			'json' => [
				'to' => $phone_number ?? self::$test_phone,
				'text' => $message ?? 'This is a test message!',
			],
		]);

        return $response->getStatusCode();
		if($response->getStatusCode() > 399) die();
		print_r($response->getStatusCode());echo "<br />";
		$body = json_decode($response->getBody());
		print_r($body);
    }
}
