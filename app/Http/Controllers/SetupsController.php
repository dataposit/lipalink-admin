<?php

namespace App\Http\Controllers;

use App\Gateway;
use App\GatewayDetail;
use App\GatewayOperation;
use App\MasterGatewayOperations;
use App\SmsTemplateGuide;
use DB;
use Illuminate\Http\Request;

class SetupsController extends Controller
{
    public function index()
    {
        $data = [
                'sms_gateways' => Gateway::get(),
                'sms_template' => SmsTemplateGuide::first(),
                'availableVariables' => DB::select("DESCRIBE `view_sms_placeholders`"),
            ];
        return view('setups.index', $data);
    }

    public function createsetup(Request $request, $type)
    {
        $type = strtolower($type);
    	if (empty($request->all())) {
    		if ($type == 'gateway')
	    		return view('setups.creategateway', ['operations' => MasterGatewayOperations::get()]);
    	} else {
    		if ($type == 'gateway'){
	    		$objecttype = new Gateway;
	    		$data = $request->only(['provider', 'website']);
                $objecttype->fill($data);
                $objecttype->save();

                // gateway operations
                foreach ($request->input('selected') as $key => $value) {
                    $operationData = [
                        'gateway_id' => $objecttype->id,
                        'master_gateway_operation_id' => $value,
                        'action' => $request->input('action')[$key] ?? NULL,
                        'requires_auth' => $request->input('authentication')[$key] ?? NULL,
                        'route' => $request->input('route')[$key] ?? NULL,
                    ];
                    if (($operationData['action'] == null) || ($operationData['route'] == null)){
                        return back();
                    }
                    $gatewayOperations = new GatewayOperation;
                    $gatewayOperations->fill($operationData);
                    $gatewayOperations->save();
                }
    		}
	    	return redirect('setups');
    	}
    }

    public function updatesetup(Request $request, $id, $type)
    {
    	$type = strtolower($type);
    	if (empty($request->all())) {
    		if ($type == 'gateway')
    			$gateway = Gateway::find($id);
	    		return view('setups.creategateway', ['gateway' => $gateway, 'operations' => MasterGatewayOperations::get()]);	
    	} else {
	    	if ($type == 'gateway'){
	    		$objecttype = Gateway::find($id);
	    		$data = $request->only(['provider', 'website']);
			}
			$objecttype->fill($data);
			$objecttype->save();
	    	return redirect('setups');
	    }
	    return back();
        // this is just a comment
    }

    public function updatesmstemplateguide(Request $request)
    {
        $SmsTemplateGuide = SmsTemplateGuide::get();
        if ($SmsTemplateGuide->isEmpty())
            $SmsTemplateGuide = new SmsTemplateGuide;
        else
            $SmsTemplateGuide = $SmsTemplateGuide->first();

        $SmsTemplateGuide->template_guide = $request->input('template');
        $SmsTemplateGuide->save();

        return back();
    }
}
