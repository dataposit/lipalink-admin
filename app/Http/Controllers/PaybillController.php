<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shortcode;
use App\Organization;
use App\ShortcodeType;
use App\Mpesa;
use App\SMS;

class PaybillController extends Controller
{
    private $message = "Thank you for paying through the test gateway";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isSysAdmin()){
            $data = Shortcode::get();
        } else {
            $data = auth()->user()->organization->paybills;
        }
        $data = (object) $data;
        return view('shortcodes.show', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->isSysAdmin()){
            $data['organizations'] = Organization::get();
            $data['types'] = ShortcodeType::get();
            $data = (object) $data;
            return view('shortcodes.form', compact('data'));
        } else {
            return view('shortcodes.form'); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('shortcode', 'short_name', 'shortcode_type_id', 'organization_id','consumer_key', 'consumer_secret', 'passkey');
        if (!isset($data['organization_id']))
            $data['organization_id'] = auth()->user()->organization->id;
        $data['type'] = $data['shortcode_type_id'] ?? NULL;
        $data['confirm_callback'] = env('NGROK_URL'). '/api/callbacks/confirm/' . $data['shortcode'];
        $data['validate_callback'] = env('NGROK_URL'). '/api/callbacks/validate_transaction/' . $data['shortcode'];
        $data['forward_call'] = env('NGROK_URL'). '/api/callbacks/forward_call/' . $data['shortcode'];
        $shortcode = new Shortcode;
        $shortcode->fill($data);
        $shortcode->save();
        return redirect('paybill');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['organizations'] = Organization::get();
        $data['shortcode'] = Shortcode::findOrFail($id);
        $data['types'] = ShortcodeType::get();
        $data = (object)$data;
        return view('shortcodes.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('shortcode', 'short_name', 'shortcode_type_id', 'organization_id','consumer_key', 'consumer_secret');
        $shortcode = Shortcode::findOrFail($id);
        $shortcode->registered = 0;
        $shortcode->fill($data);
        $shortcode->save();

        return redirect('paybill');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registercallback($shortcode) {
        $shortcode = Shortcode::where('shortcode', $shortcode)->get();
        if ($shortcode->isEmpty())
            return back();
        $shortcode = $shortcode->first();
        $response = Mpesa::register_callback($shortcode);
        // dd($shortcode);
        if ($response == 200){
            $shortcode->registered = 1;
            $shortcode->save();
        }
        return back();
    }

    public function testtransaction($shortcode) {
        $shortcode = Shortcode::where('shortcode', $shortcode)->get();
        if ($shortcode->isEmpty())
            return back();
        $shortcode = $shortcode->first();
        return view('shortcodes.testtransaction', compact('shortcode'));
    }

    public function pushtest(Request $request) {
         $shortcode = Shortcode::where('shortcode', $request->input('shortcode'))->get();
        if ($shortcode->isEmpty())
            return back();
        $shortcode = $shortcode->first();
        $response = Mpesa::sim_c2b($shortcode, $request->input('telephone'), $request->input('amount'));
        
        if ($response)
            SMS::testsms($request->input('telephone'), $this->message . " to the short code " . $request->input('shortcode'));
        return redirect('transactions');
    }
}
