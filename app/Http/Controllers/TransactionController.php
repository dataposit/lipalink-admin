<?php

namespace App\Http\Controllers;
use App\Payment;
use App\Mpesa;
use App\SMS;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function get_all() {
    	$transactions = new Payment;
        $transactions = $transactions->getTransactions();
    	return view('transactions.show', compact('transactions'));
    }

    public function testmpesaonline(Request $request) {
    	if ($request->method() == 'POST'){
    		$online = Mpesa::sim($request->input('telephone'), $request->input('amount'));
    		return redirect('transactions');
    	}
    	return view('transactions.online');
    }

    public function testpage() {
        return view('transactions.test');
    }
}
