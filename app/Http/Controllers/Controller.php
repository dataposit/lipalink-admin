<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function randomPasswordGenerator($salt = null)
    {
    	$hasmetric = $salt ?? date('Y-m-d H:i:s');
    	return substr(hash('sha256', $hasmetric), rand(0, 48), 8);
    }
}
