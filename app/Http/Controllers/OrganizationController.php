<?php

namespace App\Http\Controllers;

use App\NavDetails;
use App\Organization;
use App\UserType;
use App\User;
use App\Mail\UserCreated;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Organization::get();
        return view('organization.show', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organization.form', [
                        'user_types' => UserType::where('level', 2)->get(),
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
                            'organization_name' => ['required', 'string', 'unique:organizations,organization_name'],
                            'address' => ['required', 'string'],
                            'name' => ['required', 'string', 'max:255'],
                            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                            'user_type_id' => ['required', 'integer'],
                        ]);

        $org = new Organization;
        $org->fill($request->only(['organization_name','address', 'push_sms', 'push_nav']))->save();

        $pass = $this->randomPasswordGenerator($request->input('email'));
        $user = new User;
        $user->fill($request->only(['name', 'email', 'user_type_id']));
        $user->password = $pass;
        $user->organization_id = $org->id;
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->save();
        Mail::to([$request->input('email')])->send(new UserCreated($user, $pass));
        
        return redirect('organization');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        return view('organization.details', compact('organization'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        return view('organization.details-form', ['organization' => $organization]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {
        $service_type = strtoupper($request->input('service_type'));
        if (empty($organization->navDetails->toArray())) {
           $navDetails = new NavDetails; 
        } else {
            $navDetails = $organization->navDetails;
        }        
        $navDetails->fill($request->only([
                        'NTLM_URI','NTLM_PORT','NTLM_SERVICE','NTLM_ODATA_VERSION','NTLM_COMPANY','NTLM_WEBSERVICE','NTLM_USERNAME','NTLM_PASSWORD'
                    ]));
        $navDetails->organization_id = $organization->id;
        if ($service_type == 'ODATA')
            $navDetails->NTLM_ODATA = 1;
        else
            $navDetails->NTLM_SOAP = 1;
        $navDetails->save();
        
        $organization->enableNav();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateorgsetup(Request $request) {        
        $organization = Organization::find($request->input('organization'));
        if (empty($organization))
            return response()->json(null);
        if ($request->has('push_sms'))
            $organization->push_sms = $request->input('push_sms');
        if ($request->has('push_nav'))
            $organization->push_nav = $request->input('push_nav');
        $organization->save();
        return response()->json($organization);
    }

    public function details(Request $request)
    {
        if ($request->method() == 'POST') {
            dd($request->all());
            $navDetails = new NavDetails;
            $navDetails->fill($request->all());
            $navDetails->save();
            $organization = $navDetails->organization;
            $organization->enableNav();
        }
    }

    public function checkdetails(Request $request)
    {
        return response()->json(Organization::findOrFail($request->input('organization'))->navDetails);

    }



    
// use Illuminate\Auth\Events\Registered;


//     /**
//      * Handle a registration request for the application.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @return \Illuminate\Http\Response
//      */
//     public function register(Request $request)
//     {
//         $this->validator($request->all())->validate();

//         event(new Registered($user = $this->create($request->all())));

//         $this->guard()->login($user);

//         return $this->registered($request, $user)
//                         ?: redirect($this->redirectPath());
//     }
}
