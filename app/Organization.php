<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = ['organization_name', 'address'];    

    public function paybills() {
        return $this->hasMany('App\Shortcode');
    }

    public function smsGateways()
    {
    	return $this->hasMany('App\GatewayDetail', 'organization_id');
    }

    public function navDetails()
    {
        return $this->hasOne('App\NavDetails', 'organization_id');
    }

    public function users()
    {
        return $this->hasMany('App\User', 'organization_id');
    }

    public function transactions()
    {
        return $this->paybills->load('transactions')->pluck('transactions')->flatten();
    }

    public function adminusers()
    {
        $usertypes = new UserType;
        return $this->users->where('usertype', $usertypes->companyAdmin());
    }    

    public function enableNav()
    {
    	$this->push_nav = 1;
    	$this->save();
    }
}
