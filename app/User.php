<?php

namespace App;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Mail\UserCreated;

use Illuminate\Support\Facades\Mail;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'organization_id', 'user_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Automatically creates hash for the user password.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

    public function usertype()
    {
        return $this->belongsTo('App\UserType', 'user_type_id');
    }

    public function getLastLoginAttribute()
    {
        if (null !== $this->lastaccess)
            return date('M d, Y H:i:s', strtotime($this->lastaccess));
        return null;
    }

    public function isAllowed()
    {
        if ($this->user_type_id == 1)
            return true;
        return false;
    }

    public function set_last_access()
    {
        $this->lastaccess = date('Y-m-d H:i:s');
        $this->save();
    }

    public function saveAndNotify()
    {
        $password = substr($this->generateRandomPassword(),8);
        $this->password = $password;
        
        if ($this->save())
            return Mail::to([$this->email])->send(new UserCreated($this, $password));
    }

    private function generateRandomPassword()
    {
        return sha1(env('SALT_PHRASE').date('Y-m-d H:i:s').rand(0,2984973872).env('HASH_PHRASE'));
    }
}
