<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('eazzy:keys', function () {
    $this->comment(\App\Eazzy::create_keys());
})->describe('Create keys for Eazzy pay.');

Artisan::command('eazzy:authenticate', function () {
    $this->comment(\App\Eazzy::authenticate());
})->describe('Authenticate to eazzy platform.');
