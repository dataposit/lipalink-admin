<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/login');

Route::get('reset_password/{token}', ['as' => 'password.reset', function($token)
{
    // implement your reset password route here!
}]);

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();
Auth::routes([
	'verify' => true,
	'register' => false
]);

Route::middleware(['auth', 'verified'])->group(function(){
	Route::get('/home', 'HomeController@index')->name('home');
	// Route::any('navdetails');

	Route::resource('organization', 'OrganizationController');

	/*
	 * Setup Routes
	 */
	Route::any('createsetup/{type}', 'SetupsController@createsetup');
	Route::get('setups', 'SetupsController@index');
	Route::any('updatesetup/{setup}/{type}', 'SetupsController@updatesetup');
	Route::post('updatesmstemplateguide', 'SetupsController@updatesmstemplateguide');

	// Route::get('/registercallback/{shortcode}', 'PaybillController@registercallback');
	// Route::get('/testtransaction/{shortcode}', 'PaybillController@testtransaction');
	// Route::post('submittesttransaction', 'PaybillController@pushtest');

	// Route::get('transactions', 'TransactionController@get_all');
	// Route::get('tests', 'TransactionController@testpage');
	// Route::get('testmpesaonline', 'TransactionController@testmpesaonline');
	// Route::post('testmpesaonline', 'TransactionController@testmpesaonline');

	// Route::resource('organization', 'OrganizationController');
	// Route::resource('paybill', 'PaybillController');
	
	
	// Route::any('creategateway', 'SetupsController@createcustomerGateway');
	// Route::any('customer/{gatewaydetails}/updategateway', 'SetupsController@updategateway');
	Route::post('checkdetails', 'OrganizationController@checkdetails');
	Route::post('updateorgsetup', 'OrganizationController@updateorgsetup');

	Route::prefix('users')->name('users.')->group(function(){
		Route::resource('/', 'UserController');
	});
});