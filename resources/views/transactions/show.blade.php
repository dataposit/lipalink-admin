@extends('layouts.app')

@section('css_scripts')
    <link href="{{ asset('template/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Show Short Codes</h5>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
        	        <thead>
        		        <tr>
        		            <th>#</th>
        		            <th>Payment Code</th>
                            <th>Bill Reference</th>
                            <th>Merchant Request ID</th>
                            <th>Checkout Request ID</th>
                            <th>Mobile</th>
                            <th>Amount</th>
                            <th>Payment Time</th>
                            <th>Initiated Time</th>
        		        </tr>
        	        </thead>
        	        <tbody>
        	        @foreach($transactions as $key => $transaction)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $transaction->payment_code ?? '' }}</td>
                            <td>{{ $transaction->bill_reference ?? '' }}</td>
                            <td>{{ $transaction->merchant_request_id ?? '' }}</td>
                            <td>{{ $transaction->checkout_request_id ?? '' }}</td>
                            <td>{{ $transaction->mobile ?? '' }}</td>
                            <td>{{ $transaction->payment_amount ?? '' }}</td>
                            <td>{{ $transaction->payment_time ?? '' }}</td>
                            <td>{{ $transaction->created_at ?? '' }}</td>
                        </tr>
                    @endforeach
        	        </tbody>
        	        <tfoot>
        		        <tr>
        		            <th>#</th>
                            <th>Payment Code</th>
                            <th>Bill Reference</th>
                            <th>Merchant Request ID</th>
                            <th>Checkout Request ID</th>
                            <th>Mobile</th>
                            <th>Amount</th>
                            <th>Payment Time</th>
        		        </tr>
        	        </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('js_scripts')
    <script src="{{ asset('template/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('template/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
@endsection