@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Show Short Codes</h5>
        </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-xs-6">
                    <center>
                        <a href="{{ url('testmpesaonline') }}" class="btn btn-primary">Test Mpesa Online</a>
                    </center>
                </div>
                <div class="col-xs-6">
                    <center>
                        <a href="{{ url('testtransaction/'.env('MPESA_SHORTCODE_TWO')) }}" class="btn btn-success">Test Transaction</a>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection