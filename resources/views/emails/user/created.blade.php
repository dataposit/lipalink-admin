@component('mail::message')
# User Creation Details

Hi {{ $user->name }},<br />

An account has been created for you on {{ env('APP_NAME') }}, with the details below.<br />
You can copy the link or click on the button below to login.<br/><br/>
<strong>Email: {{ $user->email }}</strong><br />
<strong>Password: {{ $pass }}</strong><br />

Do not share these details with anyone

@component('mail::button', ['url' => env('APP_URL')])
Login
@endcomponent

Thanks,<br>
{{ env('CLIENT_APP') }}
@endcomponent
