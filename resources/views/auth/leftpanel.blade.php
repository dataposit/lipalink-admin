<style type="text/css">
	#panel-image {
		@if($page == 'login')
			background: url('{{ asset('img/login-image.png') }}') 50% 0 no-repeat;
		@elseif($page == 'register')
			background: url('{{ asset('img/register-image.png') }}') 50% 0 no-repeat;
		@else
			background: url('{{ asset('img/login-image.png') }}') 50% 0 no-repeat;
		@endif
	}
	.text-white {
		color: white;
	}

    .navy-line-light {
        width: 250px;
        margin-top: 1em;
        margin-bottom: 1em;
        border-bottom: 2px solid #FFFFFF;
    }

</style>
<div class="col-md-6" id="panel-image" style="padding-right: 0px;padding-left: 0px;">
	<div id="page-wrapper" style="width: 100%;">
		<div class="row">
			<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;background: none; padding-top: 1em;">
				<ul class="nav navbar-top-links navbar-right" style="padding: 2px 10px 2px 60px;">
					<li><span class="m-r-sm text-white">HOME</span></li>
					<li><span class="m-r-sm text-white">WHO WE ARE</span></li>
					<li><span class="m-r-sm text-white">HOW IT WORKS</span></li>
					<li><span class="m-r-sm text-white">SERVICES</span></li>
					<li><span class="m-r-sm text-white">PRICING</span></li>
					<li><span class="m-r-sm text-white">CONTACT</span></li>
				</ul>
			</nav>
		</div>
		@if($page == 'register')
		<div class="wrapper wrapper-content" style="padding-top: 20%;">
			<div class="row">
		        <div class="col-lg-12 text-center">
		          	<img src="{{ asset('img/logo.png') }}" style="width: 130px;height: 130px;" />  
		            <center><div class="navy-line-light"></div></center>
		            <h1 class="text-white">Welcome to lipalink</h1>
		        </div>
		    </div>
        </div>
        @endif
		<div class="footer" style="background: none;border-top: none;">
			<center>
				<div>
					<strong class="text-white">{{ env('OWNER_ORGANIZATION') }}</strong>
					<small class="text-white"> &copy; {{ date('Y') }}</small>
				</div>
			</center>
			
		</div>
	</div>
</div>