@extends('layouts.app', ['page' => 'login'])

@section('custom_css')
<style type="text/css">
    #login-image {
        background: url('{{ asset('img/login-image.png') }}') 50% 0 no-repeat;
    }
</style>
@endsection

@section('content')
<div class="col-md-6" style="padding-left: 8%;padding-right: 8%; padding-top: 2em;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <center>
                <img src="{{ asset('img/logo.png') }}" style="width: 70px;height: 70px;" />
                <h1>Welcome back</h1>
                <p>Please Login to your Account</p>
                </center>
                <form class="m-t" role="form" action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="current-password">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}" class="text-center">
                                    <small>{{ __('Forgot Your Password?') }}</small>
                                </a>
                            @endif
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-warning block m-b">Login</button>
                </form>
                <p class="m-t">
                    <small> &copy; {{ date('Y') }}</small>
                </p>
            </div>
        </div>
    </div>
    <center>
        
    </center>
</div>
@endsection
