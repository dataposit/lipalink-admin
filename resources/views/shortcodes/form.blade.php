@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Show Short Codes</h5>
        </div>
        <div class="ibox-content">
            <form class="m-t" role="form" action="
                    @isset($data->shortcode)
                        {{ url('paybill/'.$data->shortcode->id) }}
                    @else
                        {{ url('paybill') }}
                    @endisset" method="post">
    			@csrf
                @isset($data->shortcode)
                    @method('PUT')
                @endisset
        		<div class="form-group  row"><label class="col-sm-2 col-form-label">Business Short Code</label>
                    <div class="col-sm-10">
                    	<input type="text" name="shortcode" id="shortcode" class="form-control" value="{{ $data->shortcode->shortcode ?? '' }}">
                    </div>
                </div>
        		<div class="form-group  row"><label class="col-sm-2 col-form-label">Business name</label>
                    <div class="col-sm-10">
                    	<input type="text" name="short_name" id="short_name" class="form-control" value="{{ $data->shortcode->short_name ?? '' }}">
                    </div>
                </div>
                @if (Auth::user()->isSysAdmin())
        		<div class="form-group  row"><label class="col-sm-2 col-form-label">Organization</label>
                    <div class="col-sm-10">
                    	<select name="organization_id" id="organization_id" class="form-control">
                    	@foreach($data->organizations as $org)
                    		<option value="{{ $org->id }}"
                    			@if(isset($data->shortcode) && $data->shortcode->organization_id == $org->id)
                    				selected = "true"
                    			@endif>{{ $org->organization_name }}</option>
                    	@endforeach
                    	</select>
                    </div>
                </div>
                @endif
                <div class="form-group  row"><label class="col-sm-2 col-form-label">Consumer Key</label>
                    <div class="col-sm-10">
                       <input type="text" name="consumer_key" id="consumer_key" class="form-control" value="{{ $data->shortcode->consumer_key ?? '' }}">
                    </div>
                </div>
                <div class="form-group  row"><label class="col-sm-2 col-form-label">Consumer Secret</label>
                    <div class="col-sm-10">
                       <input type="text" name="consumer_secret" id="consumer_secret" class="form-control" value="{{ $data->shortcode->consumer_secret ?? '' }}">
                    </div>
                </div>
                <div class="form-group  row"><label class="col-sm-2 col-form-label">Pass Key</label>
                    <div class="col-sm-10">
                       <input type="text" name="passkey" id="passkey" class="form-control" value="{{ $data->shortcode->passkey ?? '' }}">
                    </div>
                </div>
                <div class="form-group  row">
                	<button class="btn btn-primary" type="submit">
                		{{ __('Submit') }}
                	</button>
                </div>
        	</form>
        </div>
    </div>
</div>
</div>

@endsection