@extends('layouts.app')

@section('css_scripts')
    <link href="{{ asset('template/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Show Short Codes</h5>
        </div>
        <div class="ibox-content">
        	<div class="row">
        		<a href="{{ url('paybill/create') }}" class="btn btn-primary"> Create Shortcode</a>
        	</div>

            <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables-example" >
	        <thead>
		        <tr>
		            <th>#</th>
		            <th>Short Code</th>
		            <th>Short Code Name</th>
		            <th>Organization</th>
		            <th>Confimation URL</th>
		            <th>Validation URL</th>
		            <th>Forward URL</th>
                    <th>Register Callback</th>
                    <th>Action</th>
		        </tr>
	        </thead>
	        <tbody>
	        @forelse($data as $key => $code)
	        	<tr class="">
		            <td>{{  $key+1 }}</td>
		            <td>{{ $code->shortcode}}</td>
		            <td>{{ $code->short_name ?? '' }}</td>
		            <td>{{ $code->organization ?? '' }}</td>
		            <td>{{ $code->confirm_callback ?? '' }}</td>
		            <td>{{ $code->validate_callback ?? '' }}</td>
		            <td>{{ $code->forward_call ?? '' }}</td>
                    <td>
                    @if($code->registered == 0)
                        <a href="{{ url('registercallback/'.$code->shortcode) }}" class="btn btn-primary">Register Callback</a>
                    @else
                        <span class="label label-success">Registered</span>
                    @endif
                    </td>
                    <td>
                        <a href="{{ url('paybill/'.$code->id.'/edit') }}" class="btn btn-primary">Update</a>
                    </td>
		        </tr>
	        @empty
	         	{{-- <tr><td colspan="2">No Data Available</td></tr> --}}
	        @endforelse
	        </tbody>
	        <tfoot>
		        <tr>
		            <th>#</th>
		            <th>Short Code</th>
		            <th>Short Code Name</th>
		            <th>Organization</th>
		            <th>Confimation URL</th>
		            <th>Validation URL</th>
		            <th>Forward URL</th>
                    <th>Register Callback</th>
                    <th>Action</th>
		        </tr>
	        </tfoot>
        </table>
            </div>

        </div>
    </div>
</div>
</div>
@endsection

@section('js_scripts')
    <script src="{{ asset('template/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('template/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
@endsection