@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Show Short Codes</h5>
            </div>
            <div class="ibox-content">
            	<form class="m-t" role="form" action="{{ url('submittesttransaction') }}" method="post">
        			@csrf
            		<div class="form-group  row"><label class="col-sm-2 col-form-label">Short Code</label>
                        <div class="col-sm-10">
                        	<input type="text" name="shortcode" id="shortcode" class="form-control" value="{{ $shortcode->shortcode ?? '' }}" required />
                        </div>
                    </div>
            		<div class="form-group  row"><label class="col-sm-2 col-form-label">Phone</label>
                        <div class="col-sm-10">
                        	<input type="text" name="telephone" id="telephone" class="form-control" required />
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Amount</label>
                        <div class="col-sm-10">
                            <input type="text" name="amount" id="amount" class="form-control" required />
                        </div>
                    </div>
                    <div class="form-group  row">
                    	<button class="btn btn-primary" type="submit">
                    		{{ __('Submit') }}
                    	</button>
                    </div>
            	</form>
            </div>
        </div>
    </div>
</div>
@endsection