@extends('layouts.app')

@section('css_scripts')
    
@endsection

@section('content')
<div class="row">
<div class="col-lg-12">
        <form class="m-t" role="form" action="{{ route('users.store') }}" method="post">
            @csrf
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>{{ trans_choice('app.general.add', 1) }}&nbsp;{{ trans_choice('app.user.user', 1) }}</h5>
                </div>
                <div class="ibox-content">
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">{{ trans_choice('app.user.name',1) }}</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') ?? $user->name ?? '' }}">
                        </div>
                    </div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">{{ trans_choice('app.user.email',1) }}&nbsp;{{ trans_choice('app.user.address',1) }}</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" id="email" class="form-control" value="{{ old('email') ?? $user->email ?? '' }}">
                        </div>
                    </div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">{{ trans_choice('app.user.role',1) }}</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="user_type_id" id="role">
                                <option disabled="true" selected="true">{{ trans_choice('app.general.select',1) }}&nbsp;{{ trans_choice('app.user.role',1) }}</option>
                                @foreach($roles as $key => $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row" id="organization" style="display: none;">
                        <label class="col-sm-2 col-form-label">{{ trans_choice('app.organization.name',1) }}</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="organization_id">
                                <option disabled="true" selected="true">{{ trans_choice('app.general.select',1) }}&nbsp;{{ trans_choice('app.organization.organization',1) }}</option>
                                @foreach($organizations as $key => $organization)
                                    <option value="{{ $organization->id }}">{{ $organization->organization_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="form-group  row">
                        <button class="btn btn-primary" type="submit">
                            {{ __('Create') }}&nbsp;{{ trans_choice('app.user.user', 1) }}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js_scripts')
    <script>
        $(document).ready(function(){
            $("#role").change(function(){
                role = $(this).val();
                if ((role == 2 || role == '2') || (role == 3 || role == '3')) {
                    $("#organization").show();
                } else {
                    $("#organization").hide();
                }
            });
        });
    </script>
@endsection