@extends('layouts.app')

@section('css_scripts')
    <link href="{{ asset('template/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
    	<div class="ibox-title">
            <h5>{{ trans_choice('app.user.user', 2) }}</h5>
        </div>
        <div class="ibox-content">
        	<a href="{{ route('users.create') }}">
        		<button class="btn btn-primary pull-right" style="margin-right: 1.5em; margin-bottom: 1em;">Create User</button>
        	</a>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
        	        <thead>
        		        <tr>
        		            <th>#</th>
        		            <th>Name</th>
                            <th>E-Mail</th>
                            <th>Role</th>
                            <th>Organization</th>
                            <th>Last Login</th>
        		        </tr>
        	        </thead>
        	        <tbody>
        	        @php
        	        	$count = 0;
        	        @endphp
        	        @foreach($users as $key => $user)
        	        @php
        	        	$count++;
        	        @endphp
                        <tr>
                           <td>{{ $count }}</td>
                           <td>{{ $user->name ?? '' }}</td>
                           <td>{{ $user->email ?? '' }}</td>
                           <td>{{ $user->usertype->name ?? '' }}</td>
                           <td>{{ $user->organization->organization_name ?? '' }}</td>
                           <td>{{ $user->last_login }}</td>
                        </tr>
                    @endforeach
        	        </tbody>
        	        <tfoot>
        		        <tr>
        		            <th>#</th>
        		            <th>Name</th>
                            <th>E-Mail</th>
                            <th>Role</th>
                            <th>Organization</th>
                            <th>Last Login</th>
        		        </tr>
        	        </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('js_scripts')
    <script src="{{ asset('template/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('template/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>
@endsection