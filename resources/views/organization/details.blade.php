@extends('layouts.app')

@section('css_scripts')

@endsection

@section('content')
<div class="row m-b-lg m-t-lg">
    <div class="col-md-6">
        {{-- <div class="profile-image">
            <img src="{{ url('template/img/a4.jpg') }}" class="rounded-circle circle-border m-b-md" alt="profile">
        </div> --}}
        <div class="profile-info">
            <div class="">
                <div>
                    <h2 class="no-margins">
                        {{ $organization->organization_name }}
                    </h2>
                    <h4></h4>
                    <small>
                        {{ $organization->address }}
                    </small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <table class="table small m-b-xs">
            <tbody>
            <tr>
                <td>
                    <strong>{{ $organization->users->count() }}</strong> {{ trans_choice('app.user', $organization->users->count()) }}
                </td>
                <td>
                    <strong>{{ $organization->adminusers()->count() }}</strong> {{ trans_choice('app.admin', 1)}} {{ trans_choice('app.user', $organization->adminusers()->count()) }}
                </td>

            </tr>
            <tr>
                <td>
                    <strong>{{ trans_choice('app.organization.sms', 1) }}</strong>&nbsp;
                    @php
                        $label = 'danger';
                        $state = 'Off';
                        if($organization->push_sms == 1) {
                            $label = 'success';
                            $state = 'On';
                        }
                    @endphp
                    <span class="label label-{{ $label }}">{{ $state }}</span>
                </td>
                <td>
                    <strong>{{ trans_choice('app.organization.nav', 1) }}</strong>&nbsp;
                    @php
                        $label = 'danger';
                        $state = 'Off';
                        if($organization->push_nav == 1) {
                            $label = 'success';
                            $state = 'On';
                        }
                    @endphp
                    <span class="label label-{{ $label }}">{{ $state }}</span>
                </td>

            </tr>
            <tr>
                <td>
                    <strong>{{ $organization->paybills->count() }}</strong> {{ trans_choice('app.shortcode', $organization->paybills->count()) }}
                </td>
                <td>
                    <strong>{{ $organization->transactions()->count() }}</strong> {{ trans_choice('app.transaction.name', $organization->transactions()->count()) }}
                </td>
            </tr>
            <tr>
                <td>
                    <strong>{{ trans_choice('app.transaction.today', 2) }}&nbsp;{{ trans_choice('app.transaction.name', $organization->transactions()->where('payment_date', date('Y-m-d'))->count()) }}</strong>
                </td>
                <td>
                    {{ $organization->transactions()->where('payment_date', date('Y-m-d'))->count() }}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-3">
        <small>{{ trans_choice('app.transaction.name', $organization->transactions()->where('payment_date', date('Y-m-d'))->count()) }} {{ trans_choice('app.transaction.today', 1) }} </small>
        <h2 class="no-margins">{{ number_format($organization->transactions()->where('payment_date', date('Y-m-d'))->count()) }}</h2>
        <div id="sparkline1"></div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="ibox">
            <div class="ibox-title"><h3>{{ trans_choice('app.shortcode', $organization->paybills->count()) }}</h3></div>
            <div class="ibox-content">
                <table class="table table-hover">
                    <tbody>
                    @foreach($organization->paybills as $key => $shortcode)
                        <tr>
                            <td><strong>{{ $shortcode->shortcode }}</strong></td>
                            <td>
                            @if($shortcode->registered)
                                <label class="label label-info">Registered</label>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="ibox">
            <div class="ibox-title">
                <h3>{{ trans_choice('app.organization.navdata', 1) }}</h3>
            </div>
            <div class="ibox-content">
                @isset($organization->navDetails)
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>
                                    @lang('app.organization.navdetails.uri')
                                </th>
                                <td>
                                    {{ $organization->navDetails->NTLM_URI ?? '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    @lang('app.organization.navdetails.port')
                                </th>
                                <td>
                                    {{ $organization->navDetails->NTLM_PORT ?? '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    @lang('app.organization.navdetails.service')
                                </th>
                                <td>
                                    {{ $organization->navDetails->NTLM_SERVICE ?? '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    @lang('app.organization.navdetails.odataversion')
                                </th>
                                <td>
                                    {{ $organization->navDetails->NTLM_ODATA_VERSION ?? '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    @lang('app.organization.navdetails.company')
                                </th>
                                <td>
                                    {{ $organization->navDetails->NTLM_COMPANY ?? '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    @lang('app.organization.navdetails.webservice')
                                </th>
                                <td>
                                    {{ $organization->navDetails->NTLM_WEBSERVICE ?? '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    @lang('app.organization.navdetails.username')
                                </th>
                                <td>
                                    {{ $organization->navDetails->NTLM_USERNAME ?? '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    @lang('app.organization.navdetails.password')
                                </th>
                                <td>
                                    {{ $organization->navDetails->NTLM_PASSWORD ?? '' }}
                                </td>
                            </tr>
                        </tbody>
                    </table>                    
                @else
                    <div class="text-center">
                        <h3 class="font-bold">Nav Details not updated</h3>
                        <div class="error-desc">
                            This company does not have NAV data. Please click on the button below to update the details.
                            <br/>
                        </div>
                    </div>                    
                @endisset
                <div class="text-center">
                    <div class="error-desc">
                        <a href="{{ url('organization/' . $organization->id . '/edit') }}" class="btn btn-primary m-t">Update Nav Details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="ibox">
            <div class="ibox-title"><h3>{{ trans_choice('app.organization.smsdata',1) }}</h3></div>
            <div class="ibox-content">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>MPESA Code</th>
                            <th>Shortcode</th>
                            <th>Account</th>
                            <th>Amount</th>
                            <th>Payment Time</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($organization->transactions()->where('payment_date', date('Y-m-d')) as $key => $transaction)
                        <tr>
                            <td>{{ $transaction->payment_code ?? '' }}</td>
                            <td>{{ $transaction->shortcode->shortcode ?? '' }}</td>
                            <td>{{ $transaction->bill_reference ?? '' }}</td>
                            <td>{{ $transaction->payment_amount ?? '' }}</td>
                            <td>{{ $transaction->payment_time ?? '' }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="ibox">
            <div class="ibox-title"><h3>{{ trans_choice('app.transaction.name', $organization->transactions()->where('payment_date', date('Y-m-d'))->count()) }}</h3></div>
            <div class="ibox-content">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>MPESA Code</th>
                            <th>Shortcode</th>
                            <th>Account</th>
                            <th>Amount</th>
                            <th>Payment Time</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($organization->transactions()->where('payment_date', date('Y-m-d')) as $key => $transaction)
                        <tr>
                            <td>{{ $transaction->payment_code ?? '' }}</td>
                            <td>{{ $transaction->shortcode->shortcode ?? '' }}</td>
                            <td>{{ $transaction->bill_reference ?? '' }}</td>
                            <td>{{ $transaction->payment_amount ?? '' }}</td>
                            <td>{{ $transaction->payment_time ?? '' }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_scripts')
    <script src="{{ asset('template/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function(){
            arrayString = JSON.parse("{{ json_encode($organization->transactions()->where('payment_date', date('Y-m-d'))->pluck('payment_amount')->toArray()) }}");

            $("#sparkline1").sparkline(arrayString, {
                type: 'line',
                width: '100%',
                height: '50',
                lineColor: '#1ab394',
                fillColor: "transparent"
            });
        });
    </script>
@endsection