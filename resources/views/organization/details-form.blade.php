@extends('layouts.app')

@section('css_scripts')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form class="m-t" role="form" action="{{ url('organization/' . $organization->id) }}" method="post">
            @csrf
            @method('put')
            <div class="ibox col-lg-12">
                <div class="ibox-title">
                    <h5>{{ $organization->organization_name }} ERP Details</h5>
                </div>
                <div class="ibox-content">
                	<input type="hidden" name="organization_id" id="organization_id" value="{{ $organization->id }}" aria-hidden="true">
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">NAV Link</label>
                        <div class="col-sm-9">
                            <input type="text" name="NTLM_URI" id="NTLM_URI" class="form-control{{ $errors->has('NTLM_URI') ? ' is-invalid' : '' }}" value="{{ old('NTLM_URI') ?? $organization->navDetails->NTLM_URI ?? '' }}" required="true">
                            @if ($errors->has('NTLM_URI'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('NTLM_URI') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">NAV Port</label>
                        <div class="col-sm-9">
                            <input type="text" name="NTLM_PORT" id="NTLM_PORT" class="form-control{{ $errors->has('NTLM_PORT') ? ' is-invalid' : '' }}" value="{{ old('NTLM_PORT') ?? $organization->navDetails->NTLM_PORT ?? '' }}" required="true">
                            @if ($errors->has('NTLM_PORT'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('NTLM_PORT') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">NAV Service Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="NTLM_SERVICE" id="NTLM_SERVICE" class="form-control{{ $errors->has('NTLM_SERVICE') ? ' is-invalid' : '' }}" value="{{ old('NTLM_SERVICE') ?? $organization->navDetails->NTLM_SERVICE ?? '' }}" required="true">
                            @if ($errors->has('NTLM_SERVICE'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('NTLM_SERVICE') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">NAV Service Type</label>
                        <div class="col-sm-9">
                            <div class="i-checks"><label> <input type="radio" value="ODATA" name="service_type"> <i></i> OData </label></div>
                            <div class="i-checks"><label> <input type="radio" value="SOAP" name="service_type" disabled="true"> <i></i> SOAP </label></div>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">NAV OData Version</label>
                        <div class="col-sm-9">
                            <input type="text" name="NTLM_ODATA_VERSION" id="NTLM_ODATA_VERSION" class="form-control{{ $errors->has('NTLM_ODATA_VERSION') ? ' is-invalid' : '' }}" value="{{ old('NTLM_ODATA_VERSION') ?? $organization->navDetails->NTLM_ODATA_VERSION ?? '' }}">
                            @if ($errors->has('NTLM_ODATA_VERSION'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('NTLM_ODATA_VERSION') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">NAV Company</label>
                        <div class="col-sm-9">
                            <input type="text" name="NTLM_COMPANY" id="NTLM_COMPANY" class="form-control{{ $errors->has('NTLM_COMPANY') ? ' is-invalid' : '' }}" value="{{ old('NTLM_COMPANY') ?? $organization->navDetails->NTLM_COMPANY ?? '' }}" required="true">
                            @if ($errors->has('NTLM_COMPANY'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('NTLM_COMPANY') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">@lang('app.organization.navdetails.webservice')</label>
                        <div class="col-sm-9">
                            <input type="text" name="NTLM_WEBSERVICE" id="NTLM_WEBSERVICE" class="form-control{{ $errors->has('NTLM_WEBSERVICE') ? ' is-invalid' : '' }}" value="{{ old('NTLM_WEBSERVICE') ?? $organization->navDetails->NTLM_WEBSERVICE ?? '' }}" required="true">
                            @if ($errors->has('NTLM_WEBSERVICE'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('NTLM_WEBSERVICE') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">NAV Username</label>
                        <div class="col-sm-9">
                            <input type="text" name="NTLM_USERNAME" id="NTLM_USERNAME" class="form-control{{ $errors->has('NTLM_USERNAME') ? ' is-invalid' : '' }}" value="{{ old('NTLM_USERNAME') ?? $organization->navDetails->NTLM_USERNAME ?? '' }}" required="true">
                            @if ($errors->has('NTLM_USERNAME'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('NTLM_USERNAME') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-3 col-form-label">NAV Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="NTLM_PASSWORD" id="NTLM_PASSWORD" class="form-control{{ $errors->has('NTLM_PASSWORD') ? ' is-invalid' : '' }}" value="{{ old('NTLM_PASSWORD') ?? $organization->navDetails->NTLM_PASSWORD ?? '' }}" required="true">
                            @if ($errors->has('NTLM_PASSWORD'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('NTLM_PASSWORD') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
            </div>
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="form-group  row">
                        <center>
                            <button class="btn btn-primary" type="submit">
                                {{ __('Submit') }}
                            </button>
                        </center>
                    </div>
                </div>
            </div>
        </form> 
    </div>
</div>

@endsection

@section('js_scripts')

@endsection

@section('custom_js')
    <script>
        $(document).ready(function(){
            
        });
    </script>
@endsection