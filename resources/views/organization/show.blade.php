@extends('layouts.app')

@section('css_scripts')
    <link href="{{ asset('template/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Show Organizations</h5>
        </div>
        <div class="ibox-content">
        	<div class="row">
        		<a href="{{ url('organization/create') }}" class="btn btn-primary"> Create Organization</a>
        	</div>
            <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables-example" >
	        <thead>
		        <tr>
		            <th>#</th>
		            <th>Organization Name</th>
                    <th>Push to ERP</th>
                    <th>Send SMS</th>
                    <th>Details</th>
		        </tr>
	        </thead>
	        <tbody>
	        @forelse($data as $key => $org)
	        	<tr class="">
		            <td>{{  $key+1 }}</td>
		            <td>{{ $org->organization_name}}</td>
                    <td>
                        <center id="push_nav{{ $org->id }}"><input type="checkbox" name="push_nav" id="push_nav{{ $org->id }}" class="js-nav-switch_{{ $org->id }}" value="{{ $org->id }}" @if($org->push_nav == 1) checked @endif></center>
                        <div class="spiner-example" id="div-nav{{ $org->id }}" style="padding-top: 10px;height: 40px;display: none;;">
                            <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <center id="push_sms{{ $org->id }}"><input type="checkbox" name="push_sms" class="js-sms-switch_{{ $org->id }}" value="{{ $org->id }}" @if($org->push_sms == 1) checked @endif></center>
                        <div class="spiner-example" id="div-sms{{ $org->id }}" style="padding-top: 10px;height: 40px;display: none;">
                            <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                        </div>
                    </td>
                    <td><a href="{{ url('organization/' . $org->id) }}" class="btn btn-info">Details</a></td>
		        </tr>
	        @empty
	         	{{-- <tr><td colspan="2">No Data Available</td></tr> --}}
	        @endforelse
	        </tbody>
	        <tfoot>
		        <tr>
		            <th>#</th>
		            <th>Organization Name</th>
                    <th>Push to ERP</th>
                    <th>Send SMS</th>
                    <th>Details</th>
		        </tr>
	        </tfoot>
        </table>
            </div>

        </div>
    </div>
</div>
</div>
<div class="modal inmodal fade" id="NavModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="m-t" role="form" action="{{ url('organization') }}" method="post" id="nav-details-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">NAV Details</h4>
                    <small class="font-bold">Please provide/confirm the NAV details below.</small>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="organization" id="organization" aria-hidden="true">
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">NAV LINK</label>
                        <div class="col-sm-10">
                            <input type="text" name="nav_uri" id="nav_uri" class="form-control{{ $errors->has('nav_uri') ? ' is-invalid' : '' }}" value="{{ old('nav_uri') }}" required="true">
                            @if ($errors->has('nav_uri'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nav_uri') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">NAV Username</label>
                        <div class="col-sm-10">
                            <input type="text" name="nav_username" id="nav_username" class="form-control{{ $errors->has('nav_username') ? ' is-invalid' : '' }}" value="{{ old('nav_username') }}" required="true">
                            @if ($errors->has('nav_username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nav_username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">NAV Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="nav_password" id="nav_password" class="form-control{{ $errors->has('nav_password') ? ' is-invalid' : '' }}" value="{{ old('nav_password') }}" required="true">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js_scripts')
    <script src="{{ asset('template/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('template/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('template/js/plugins/switchery/switchery.js') }}"></script>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });
            @foreach($data as $key => $org)
                var elem = document.querySelector('.js-nav-switch_{{ $org->id }}');
                var switchery = new Switchery(elem, { color: '#1AB394' });

                var elem = document.querySelector('.js-sms-switch_{{ $org->id }}');
                var switchery = new Switchery(elem, { color: '#1AB394' });
            @endforeach

            $('input[name="push_sms"]').change(function(){
                elementVal = $(this).val();
                $("#div-sms"+elementVal).show();
                $("#push_sms"+elementVal).hide();
                checked = $(this).prop('checked');
                value = 0;
                if (checked === true){
                    value = 1;
                }
                data = {'organization' : $(this).val(), 'push_sms' : value, '_token': $('meta[name=csrf-token]').attr('content')};
                // console.log(data);
                $.post("{{ url('updateorgsetup') }}", data, function(data){
                    if (data == null) {
                        if (checked === true) {
                            $(this).attr('checked', 'false');
                        } else {
                            $(this).attr('checked', 'true');
                        }
                    }
                    $("#push_sms"+elementVal).show();
                    $("#div-sms"+elementVal).hide();
                });
            });

            $('input[name="push_nav"]').change(function(){
                elementVal = $(this).val();
                $("#div-nav"+elementVal).show();
                $("#push_nav"+elementVal).hide();
                checked = $(this).prop('checked');
                if (checked === true){
                    url = "{{ url('organization') }}" + "/" + elementVal + "/edit";
                    $.post("{{ url('checkdetails') }}", {'organization':elementVal}, function(data){
                        if (Object.entries(data).length === 0) {
                            window.location.replace(url);
                        } else {
                            changeOrgERPStatus(elementVal, $(this));
                        }
                    });
                } else {
                    changeOrgERPStatus(elementVal, $(this));
                }
            });

        });

        function changeOrgERPStatus(organization, element) {
            value = 0;
            if (checked === true){
                value = 1;
            }
            data = {'organization' : organization, 'push_nav' : value, '_token': $('meta[name=csrf-token]').attr('content')};
            $.post("{{ url('updateorgsetup') }}", data, function(data){
                    if (data == null) {
                        if (checked === true) {
                            element.attr('checked', 'false');
                        } else {
                            element.attr('checked', 'true');
                        }
                    }
                    $("#push_nav"+organization).show();
                    $("#div-nav"+organization).hide();
                });
        }

    </script>
@endsection