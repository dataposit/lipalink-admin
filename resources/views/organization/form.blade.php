@extends('layouts.app')

@section('css_scripts')
<link href="{{ asset('template/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form class="m-t" role="form" action="{{ url('organization') }}" method="post">
            @csrf
            <div class="ibox col-lg-12">
                <div class="ibox-title">
                    <h5>Register Organization</h5>
                </div>
                <div class="ibox-content">
                	<div class="form-group  row"><label class="col-sm-2 col-form-label">Organization Name</label>
                        <div class="col-sm-10">
                        	<input type="text" name="organization_name" id="organization_name" class="form-control{{ $errors->has('organization_name') ? ' is-invalid' : '' }}" value="{{ old('organization_name') ?? $data->organization->organization_name ?? '' }}" required="true">
                            @if ($errors->has('organization_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('organization_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Organization Address</label>
                        <div class="col-sm-10">
                            <input type="text" name="address" id="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" value="{{ old('address') ?? $data->organization->address ?? '' }}" required="true">
                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Send Acknowledgement SMS</label>
                        <div class="col-sm-4">
                            <input type="checkbox" name="push_sms" id="push_sms" class="js-switch" value="1" checked />
                        </div>
                        <label class="col-sm-2 col-form-label">Sych to ERP</label>
                        <div class="col-sm-4">
                            <input type="checkbox" name="push_nav" id="push_nav" class="js-switch_2" value="1" checked />
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-title">
                    <h5> Add Organization Admin Contact</h5>
                </div>
                <div class="ibox-content">
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Contact Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" required="true">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Contact Email</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required="true">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Contact User Role</label>
                        <div class="col-sm-10">
                            <select name="user_type_id" id="user_type_id" class="form-control{{ $errors->has('user_type_id') ? ' is-invalid' : '' }}" required="true">
                                <option selected="true" disabled="true">Select User Role</option>
                                @foreach($user_types as $key => $usertype)
                                    <option value="{{ $usertype->id }}">{{ $usertype->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('user_type_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('user_type_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="form-group  row">
                        <center>
                            <button class="btn btn-primary" type="submit">
                                {{ __('Submit') }}
                            </button>
                        </center>
                    </div>
                </div>
            </div>
        </form> 
    </div>
</div>

@endsection

@section('js_scripts')
<script src="{{ asset('template/js/plugins/switchery/switchery.js') }}"></script>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function(){
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });

            var elem = document.querySelector('.js-switch_2');
            var switchery = new Switchery(elem, { color: '#1AB394' });
        });
    </script>
@endsection