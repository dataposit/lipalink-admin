@extends('layouts.app')

@section('css_scripts')
    <link href="{{ asset('template/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>SMS Gateways</h5>
        </div>
        <div class="ibox-content">
        	<div class="row">
        		<a href="{{ url('createsetup/gateway') }}" class="btn btn-primary"> Create Gateway</a>
        	</div>

            <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTables-example" >
	        <thead>
		        <tr>
		            <th>#</th>
		            <th>SMS Gateway provider</th>
		            <th>Website</th>
		            <th>Action</th>
		        </tr>
	        </thead>
	        <tbody>
	        @forelse($sms_gateways as $key => $gateway)
	        	<tr class="">
		            <td>{{  $key+1 }}</td>
		            <td>{{ $gateway->provider}}</td>
		            <td>{{ $gateway->website ?? '' }}</td>
                    <td>
                        <a href="{{ url('updatesetup/'.$gateway->id.'/gateway') }}" class="btn btn-primary">Update</a>
                    </td>
		        </tr>
	        @empty
	         	{{-- <tr><td colspan="2">No Data Available</td></tr> --}}
	        @endforelse
	        </tbody>
	        <tfoot>
		        <tr>
		            <th>#</th>
		            <th>SMS Gateway provider</th>
		            <th>Website</th>
		            <th>Action</th>
		        </tr>
	        </tfoot>
        </table>
            </div>

        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h4>SMS Template Guide</h4>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <h5>Available Variables</h5>
            <ul>
            @foreach($availableVariables as $variable)
                <li>{{ '{$' . $variable->Field .'}' }}</li>
            @endforeach
            </ul>
            <hr class=""/>
            <form class="m-t no-padding" role="form" action="{{ url('updatesmstemplateguide') }}" method="post" enctype="multipart/data">
                @csrf
                <textarea class="summernote" name="template">{{ $sms_template->template_guide ?? '' }}</textarea>
                <button class="btn btn-primary" type="submit">
                    {{ __('Submit') }}
                </button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection

@section('js_scripts')
    <script src="{{ asset('template/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('template/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('template/js/plugins/summernote/summernote-bs4.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

            $('.summernote').summernote();

        });

    </script>
@endsection