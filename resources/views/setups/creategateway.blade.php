@extends('layouts.app')

@section('content')
@php
    $actions = ['post', 'get', 'put', 'delete'];
@endphp
<div class="row">
    <div class="col-lg-12">
        <form class="m-t" role="form" action="
                @isset($gateway)
                    {{ url('updatesetup/'.$gateway->id . '/gateway') }}
                @else
                    {{ url('createsetup/gateway') }}
                @endisset" method="post">
            @csrf
            @isset($gateway)
                @method('post')
            @endisset
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Create SMS Gateway</h5>
                </div>
                <div class="ibox-content">
                    
                		<div class="form-group  row"><label class="col-sm-2 col-form-label">Service Provider</label>
                            <div class="col-sm-10">
                            	<input type="text" name="provider" id="provider" class="form-control" value="{{ $gateway->provider ?? '' }}">
                            </div>
                        </div>
                		<div class="form-group  row"><label class="col-sm-2 col-form-label">Service Provider Website</label>
                            <div class="col-sm-10">
                            	<input type="text" name="website" id="website" class="form-control" value="{{ $gateway->website ?? '' }}">
                            </div>
                        </div>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>SMS Gateway Operations</h5>
                </div>
                <div class="ibox-content table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Operation</th>
                                <th>Check</th>
                                <th>Action</th>
                                <th>Authentication Required</th>
                                <th>Route</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($operations as $key => $operation)
                        @php
                            $gatewayOperation = NULL;
                            if (isset($gateway))
                                $gatewayOperation = $gateway->operations->where('master_gateway_operation_id', $operation->id);
                        @endphp
                            <tr>
                                <td>
                                    {{ $operation->operation }}
                                </td>
                                <td>
                                    <div class="i-checks"><label> <input type="checkbox" name="selected[]"  value="{{ $operation->id }}" @if(isset($gateway) && !$gatewayOperation->isEmpty()){{'checked'}}@endif><i></i> </label></div>
                                </td>
                                <td>
                                    <select class="form-control m-b" name="action[]">
                                    @foreach($actions as $action)
                                        <option value="{{$action}}" @if(isset($gateway) && $action == $gatewayOperation->first()->action){{'checked'}}@endif>{{strtoupper($action)}}</option>
                                    @endforeach
                                    </select>
                                </td>
                                <td>
                                    <div class="i-checks"><label> <input type="checkbox" name="authentication[]"  value="{{ $operation->id }}" @if(isset($gateway) && !$gatewayOperation->isEmpty() && $gatewayOperation->first()->requires_auth == 1){{'checked'}}@endif><i></i> </label></div>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="route[]" 
                                    value="
                                    @if(isset($gateway) && $gatewayOperation->isEmpty())
                                    {{$gatewayOperation->first()->route}}@endif"
                                    >
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="form-group  row">
                        <button class="btn btn-primary" type="submit">
                            {{ __('Submit') }}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection