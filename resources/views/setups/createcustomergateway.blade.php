@extends('layouts.app')

@section('content')
@php
    $actions = ['post', 'get', 'put', 'delete'];
@endphp
<div class="row">
    <div class="col-lg-12">
        <form class="m-t" role="form" action="
                @isset($gateway)
                    {{ url('customer/'.$gateway->id .'/updategateway') }}
                @else
                    {{ url('creategateway') }}
                @endisset" method="post">
            @csrf
            @isset($gateway)
                @method('post')
            @endisset
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Create SMS Gateway</h5>
                </div>
                <div class="ibox-content">
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Service Provider</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="gateway_id">
                                @if(!isset($gateway))
                                <option disabled="true" selected="true">Select Gateway</option>
                                @endif
                                @foreach($gateways as $key => $mgateway)
                                    <option @if(isset($gateway) && $gateway->id == $mgateway->id){{'selected'}}@endif value="{{ $mgateway->id }}">{{ $mgateway->provider }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            		<div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Authentication Type</label>
                        <div class="col-sm-10">
                        	<select class="form-control m-b" name="authentication_method" id="authentication_method">
                                @if(!isset($gateway))
                                <option disabled="true" selected="true">Select Gateway Authentication Method</option>
                                @endif
                                <option value="credentials" @if(isset($gateway) && null !== $gateway->username){{'selected'}}@endif>API Credentials</option>
                                <option value="key" @if(isset($gateway) && null !== $gateway->api_key){{'selected'}}@endif>API Key</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row hidden_fields authentication_credentials" @if(!isset($gateway) && null !== $gateway->username) style="display: none;" @endif>
                        <label class="col-sm-2 col-form-label">Gateway Username</label>
                        <div class="col-sm-10">
                            <input type="text" name="username" id="username" class="form-control" value="{{ $gateway->username ?? '' }}">
                        </div>
                    </div>
                    <div class="form-group  row hidden_fields authentication_credentials" @if(!isset($gateway) && null !== $gateway->username) style="display: none;" @endif>
                        <label class="col-sm-2 col-form-label">Gateway Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="password" id="password" class="form-control" value="{{ $gateway->password ?? '' }}">
                        </div>
                    </div>
                    <div class="form-group  row hidden_fields authentication_key" @if(!isset($gateway) && null !== $gateway->api_key) style="display: none;" @endif>
                        <label class="col-sm-2 col-form-label">API Key</label>
                        <div class="col-sm-10">
                            <input type="text" name="api_key" id="api_key" class="form-control" value="{{ $gateway->api_key ?? '' }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="form-group  row">
                        <button class="btn btn-primary" type="submit">
                            {{ __('Submit') }}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('js_scripts')
   <script>
        $(document).ready(function(){
            $("#authentication_method").change(function(){
                method = $(this).val();
                $(".hidden_fields").hide();
                $(".authentication_" + method).show();
            });
        });
    </script>
@endsection