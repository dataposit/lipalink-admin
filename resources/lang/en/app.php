<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'admin' => 'Admin|Admins',
    'general' => [
            'add' => 'Add',
            'select' => 'Select',
            'name' => 'Name|Names'
        ],
    'organization' => [
                'address' => 'Address|Addresses',
                'name' => 'Organization Name|Organization Names',
                'nav' => 'Push Nav',
                'navdata' => 'NAV Data',
                'navdetails' => [
                        'uri' => 'NAV URI',
                        'port' => 'NAV PORT',
                        'service' => 'NAV SERVICE',
                        'soap' => 'NAV SOAP',
                        'odata' => 'NAV ODATA',
                        'odataversion' => 'ODATA VERSION',
                        'company' => 'NAV COMPANY',
                        'username' => 'NAV USERNAME',
                        'password' => 'NAV PASSWORD',
                        'webservice' => 'WEB SERVICE',
                    ],
                'organization' => 'Organization|Organizations',
                'sms' => 'Send SMS',
                'smsdata' => 'SMS Details',
            ],
    'shortcode' => 'Shortcode|Shortcodes',
    'transaction' => [
                'name' => 'Transaction|Transactions',
                'today' => 'Today|Today`s',
            ],
    'user' => [
        'user' => 'User|Users',
        'fields' => [
            'address' => 'Address|Addresses',
            'email' => 'E-Mail',
            'name' => 'Name|Names',
            'role' => 'Role|Roles'
        ]
    ]
];
